import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { ICollection } from '@taskorg/api-interfaces';
import { catchError, map, Observable, of } from 'rxjs';
import { CollectionService } from '../services/collection.service';

export interface ICollectionsResolve {
  collections: ICollection[];
  error?: string;
}

@Injectable()
export class CollectionsResolver implements Resolve<ICollectionsResolve> {
  constructor(private collectionService: CollectionService) {}

  resolve(): Observable<ICollectionsResolve> {
    return this.collectionService
      .getCollections()
      .pipe(
        map((value) => ({
          collections: value,
          error: '',
        }))
      )
      .pipe(
        catchError(() => {
          return of({
            collections: [],
            error: 'Collections data not available at this time, retry!',
          });
        })
      );
  }
}
