import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UiService {
  constructor() {}

  // refreshPage(): void {
  //   this.collectionService
  //     .getCollections()
  //     .subscribe((res) => (this.collections = res));
  // }

  // openAddCollectionDialog(): void {
  //   const config: DynamicDialogConfig = {
  //     data: {},
  //     header: 'New Collection',
  //     modal: true,
  //     width: '500px',
  //   };
  //   this.dialogService
  //     .open(CollectionEditComponent, config)
  //     .onClose.subscribe((res: ICollection) => {
  //       if (res) {
  //         this.messageService.add({
  //           key: 'COLLECTION_TOAST',
  //           severity: 'success',
  //           detail: `Collections ${res.image} has been successfully added`,
  //         });
  //         this.refreshPage();
  //       }
  //     });
  // }
}
