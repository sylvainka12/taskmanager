import { Injectable } from '@angular/core';
import { ICollection } from '@taskorg/api-interfaces';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../../../../apps/taskmanager/src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class CollectionService {
  collectionResourceURL = environment.apiURL + '/collections';

  // private collectionsData: BehaviorSubject<ICollection[]> = new BehaviorSubject<
  //   ICollection[]
  // >([]);
  // collections = this.collectionsData.asObservable();

  constructor(private http: HttpClient) {}

  getCollections(): Observable<ICollection[]> {
    return this.http.get<ICollection[]>(`${this.collectionResourceURL}`);
  }

  // iniCollections(): void {
  //   this.getCollections().subscribe((res) => this.collectionsData.next(res));
  // }

  createCollection(body: ICollection): Observable<ICollection> {
    return this.http.post<ICollection>(`${this.collectionResourceURL}`, body);
  }
}
