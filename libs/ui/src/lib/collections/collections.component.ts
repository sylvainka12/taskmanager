import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ICollection } from '@taskorg/api-interfaces';

@Component({
  selector: 'taskorg-collections',
  templateUrl: './collections.component.html',
  styleUrls: ['./collections.component.scss'],
})
export class CollectionsComponent implements OnInit {
  @Output()
  openCollectionDialog: EventEmitter<any> = new EventEmitter();

  @Input()
  collections?: ICollection[];

  @Input()
  empty?: boolean = false;

  constructor() {}

  ngOnInit(): void {}

  openDialog(): void {
    this.openCollectionDialog?.emit();
  }
}
