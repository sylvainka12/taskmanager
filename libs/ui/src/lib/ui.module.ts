import { NgModule } from '@angular/core';
import { NavigationComponent } from './navigation/navigation.component';
import { LayoutComponent } from './layout/layout.component';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SharedModule } from '@taskorg/shared';
import { CollectionsResolver } from './resolvers/collections.resolver';
import { CollectionEditComponent } from './collection-edit/collection-edit.component';
import { DialogService } from 'primeng/dynamicdialog';
import { MessageService } from 'primeng/api';
import { CollectionsComponent } from './collections/collections.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    resolve: {
      collections: CollectionsResolver,
    },
  },
  {
    path: 'collections',
    component: LayoutComponent,
    children: [{ path: '', component: LayoutComponent }],
  },
];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [
    NavigationComponent,
    LayoutComponent,
    HomeComponent,
    CollectionEditComponent,
    CollectionsComponent,
  ],
  exports: [RouterModule],
  providers: [CollectionsResolver, DialogService, MessageService],
})
export class UiModule {}
