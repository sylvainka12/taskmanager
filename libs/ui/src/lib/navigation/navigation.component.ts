import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ICollection } from '@taskorg/api-interfaces';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { CollectionEditComponent } from '../collection-edit/collection-edit.component';

@Component({
  selector: 'taskorg-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit {
  @Output() collectionCreated: EventEmitter<ICollection> = new EventEmitter();

  constructor(private dialogService: DialogService) {}

  ngOnInit(): void {}

  openAddCollectionDialog(): void {
    const config: DynamicDialogConfig = {
      data: {},
      header: 'New Collection',
      modal: true,
      width: '500px',
    };
    this.dialogService
      .open(CollectionEditComponent, config)
      .onClose.subscribe((res: ICollection) => {
        if (res) {
          this.collectionCreated.emit(res);
        }
      });
  }
}
