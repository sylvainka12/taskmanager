import { Component, OnInit } from '@angular/core';
import {
  ActivatedRoute,
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Router,
} from '@angular/router';
import { ICollection } from '@taskorg/api-interfaces';
import { Message, MessageService } from 'primeng/api';
import { Observable, skipLast } from 'rxjs';
import { DialogService, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { CollectionEditComponent } from '../collection-edit/collection-edit.component';
import { CollectionService } from '../services/collection.service';

@Component({
  selector: 'taskorg-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  collections$?: Observable<ICollection[]>;
  collections: ICollection[] = [];
  loading = false;
  msgs: Message[] = [];
  isDataCollectionEmpty = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private collectionService: CollectionService,
    private dialogService: DialogService,
    private messageService: MessageService
  ) {
    this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationStart) {
        this.loading = true;
      }
      if (
        ev instanceof NavigationCancel ||
        ev instanceof NavigationEnd ||
        ev instanceof NavigationError
      ) {
        this.loading = false;
      }
    });
  }

  ngOnInit(): void {
    this.loadPage();
  }

  loadPage(): void {
    const data = this.route.snapshot.data['collections'];
    this.collections = data.collections;
    this.isDataCollectionEmpty =
      this.collections.length === 0 && data.error === '';

    console.log(this.isDataCollectionEmpty);

    if (data.error) {
      this.msgs.push({
        severity: 'error',
        detail: data.error,
        closable: false,
      });
    }
  }

  handleRefreshCollectionList(collection: ICollection): void {
    this.messageService.add({
      key: 'COLLECTION_TOAST',
      severity: 'success',
      detail: `Collections ${collection.image} has been successfully added`,
    });

    this.collectionService
      .getCollections()
      .subscribe((res) => (this.collections = res));
  }

  openAddCollectionDialog(): void {
    const config: DynamicDialogConfig = {
      data: {},
      header: 'New Collection',
      modal: true,
      width: '500px',
    };
    this.dialogService
      .open(CollectionEditComponent, config)
      .onClose.subscribe((res: ICollection) => {
        if (res !== undefined) this.handleRefreshCollectionList(res);
      });
  }
}
