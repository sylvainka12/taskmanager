import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ICollection, IItem } from '@taskorg/api-interfaces';
import { COLORS } from '@taskorg/shared';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { CollectionService } from '../services/collection.service';
import { ICONS } from '@taskorg/shared';

@Component({
  selector: 'taskorg-collection-edit',
  templateUrl: './collection-edit.component.html',
  styleUrls: ['./collection-edit.component.scss'],
})
export class CollectionEditComponent implements OnInit {
  editForm = this.fb.group({
    name: [[], [Validators.required]],
    image: ['', []],
    color: ['', [Validators.required]],
    description: ['', []],
  });

  colors = COLORS;
  selectedColor?: IItem;

  icons = ICONS;
  selectedIcon?: IItem;

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private collectionService: CollectionService
  ) {}

  ngOnInit(): void {
    this.selectedColor = this.colors[0];
  }

  closeDialog(): void {
    this.ref.close();
  }

  save(): void {
    const data = this.editForm.value;
    const body: ICollection = {
      name: data['name'],
      color: this.selectedColor?.code,
      image: data['image'] ? data['image'] : 'pi-table',
      description: data['description'],
    };

    this.collectionService.createCollection(body).subscribe((res) => {
      this.ref.close(res);
    });
  }
}
