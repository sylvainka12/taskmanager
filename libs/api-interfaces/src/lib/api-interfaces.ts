export interface Message {
  message: string;
}

export interface ICollection {
  id?: number;
  name?: string;
  image?: string;
  color?: string;
  description?: string;
}

export interface IItem {
  name: string;
  code: string;
}
