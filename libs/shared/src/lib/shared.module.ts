import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedLibModule } from './shared-lib.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [],
  exports: [CommonModule, FormsModule, ReactiveFormsModule, SharedLibModule],
})
export class SharedModule {}
