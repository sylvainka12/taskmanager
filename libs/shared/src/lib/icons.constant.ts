import { IItem } from '@taskorg/api-interfaces';

export const ICONS: IItem[] = [
  {
    code: 'pi-table',
    name: 'Table',
  },
  {
    code: 'pi-chart-bar',
    name: 'Chart Bar',
  },
  {
    code: 'pi-chart-pie',
    name: 'Chart Pie',
  },
  {
    code: 'pi-chart-line',
    name: 'Chart Line',
  },
  {
    code: 'pi-user',
    name: 'User',
  },
  {
    code: 'pi-book',
    name: 'Book',
  },
  {
    code: 'pi-bookmark-fill',
    name: 'Bookmark Fill',
  },
  {
    code: 'pi-bookmark',
    name: 'Bookmark',
  },
  {
    code: 'pi-building',
    name: 'Building',
  },
];
