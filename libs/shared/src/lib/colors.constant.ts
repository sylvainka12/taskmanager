import { IItem } from '@taskorg/api-interfaces';

export const COLORS: IItem[] = [
  {
    name: 'Silver',
    code: '#C0C0C0',
  },
  {
    name: 'Gray',
    code: '#808080',
  },
  {
    name: 'Black',
    code: '#000000',
  },
  {
    name: 'Red',
    code: '#FF0000',
  },
  {
    name: 'Maroon',
    code: '#800000',
  },
  {
    name: 'Yellow',
    code: '#FFFF00',
  },
  {
    name: 'Olive',
    code: '#808000',
  },
  {
    name: 'Lime',
    code: '#00FF00',
  },
  {
    name: 'Green',
    code: '#008000',
  },
  {
    name: 'Aqua',
    code: '#00FFFF',
  },
  {
    name: 'Teal',
    code: '#008080',
  },
  {
    name: 'Blue',
    code: '#0000FF',
  },
  {
    name: 'Navy',
    code: '#000080',
  },
  {
    name: 'Fuchsia',
    code: '#FF00FF',
  },
  {
    name: 'Purple',
    code: '#800080',
  },
];
