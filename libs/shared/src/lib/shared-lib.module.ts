import { NgModule } from '@angular/core';
import { ButtonModule } from 'primeng/button';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { ToastModule } from 'primeng/toast';
import { ListboxModule } from 'primeng/listbox';

@NgModule({
  declarations: [],
  exports: [
    ButtonModule,
    ProgressSpinnerModule,
    MessagesModule,
    MessageModule,
    DialogModule,
    InputTextModule,
    DropdownModule,
    ToastModule,
    ListboxModule,
  ],
})
export class SharedLibModule {}
