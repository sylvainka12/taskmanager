import { Controller, Get } from '@nestjs/common';

import { ICollection, Message } from '@taskorg/api-interfaces';

import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('hello')
  getData(): Message {
    return this.appService.getData();
  }

  // @Get('collections')
  // getCollections(): ICollection[] {
  //   return this.
  // }
}
