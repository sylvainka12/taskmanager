import { Body, Controller, Get, Post } from '@nestjs/common';
import { ICollection } from '@taskorg/api-interfaces';
import { CollectionService } from './collection.service';

@Controller('collections')
export class CollectionController {
  constructor(private collectionService: CollectionService) {}

  @Get('')
  getCollections(): Promise<ICollection[]> {
    return this.collectionService.findAll();
  }

  @Post('')
  createCollection(@Body() body: ICollection): Promise<ICollection> {
    return this.collectionService.createCollection(body);
  }
}
