import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'Collections' })
export class CollectionEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  image: string;

  @Column()
  color: string;

  @Column()
  description: string;
}
