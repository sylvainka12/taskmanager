import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { ICollection } from '@taskorg/api-interfaces';
import { Observable } from 'rxjs';
import { AxiosResponse } from 'axios';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CollectionEntity } from './entity/collection.entity';

@Injectable()
export class CollectionService {
  COLLECTION_RESOURCE_URL = '/collections';
  constructor(
    @InjectRepository(CollectionEntity)
    private collectionRepository: Repository<CollectionEntity>
  ) {}

  findAll(): Promise<CollectionEntity[]> {
    return this.collectionRepository.find();
  }

  async findOneById(id: number): Promise<CollectionEntity> {
    try {
      return await this.collectionRepository.findOneOrFail(id);
    } catch (error) {
      throw error;
    }
  }

  createCollection(collection: ICollection): Promise<CollectionEntity> {
    const newCollection = this.collectionRepository.create(collection);
    return this.collectionRepository.save(collection);
  }

  async updateCollectio(
    id: number,
    data: ICollection
  ): Promise<CollectionEntity> {
    const collection = await this.findOneById(id);
    return this.collectionRepository.save(collection);
  }
}
