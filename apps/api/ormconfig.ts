import { SqliteConnectionOptions } from 'typeorm/driver/sqlite/SqliteConnectionOptions';
import { CollectionEntity } from './src/collection/entity/collection.entity';

const config: SqliteConnectionOptions = {
  type: 'sqlite',
  database: 'db',
  entities: [CollectionEntity],
  synchronize: true,
};

export default config;
