# TaskManager APP

Taskmanager App

- Architecture: Monorepos
- Backend API NestJS (https://docs.nestjs.com/)
- Database SQLite 
- Frontend Angular (https://angular.io/)
- UI Component Library: PRIMENG (https://www.primefaces.org/primeng/)
- Build system Nx (https://nx.dev/)
- Last Release v1.0.2
  
## Start App
1. Clone project
   
   ```
    > git clone https://gitlab.com/sylvainka12/taskmanager.git
   ```
2. Go in folder
   
   ```
   > cd taskmanager
   ```
3. Install dependencies

    ```
    > npm install
    ```

4. Start API
   
   ```
   > npx nx serve api
   ```
5. Start frontend
   ```
   > npx nx serve taskmanager
   ```
### Done!!!
